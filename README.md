# Hitcat-AUR

![hitcat-aur](logo_hitcat_aur.png "hitcat-aur")

My own Arch Linux Repository (AUR) with my software and some software made by my friends, work on pacman, pamac-manager, octopi or any other pacman-based package manager, support for `x86_64` and `aarch64` architectures. This repository not depend of systemd, so you can use it with any other init system (perfect for work with Artix Linux).

(Sorry, `armv7h` discontinued).

## Add my repo to pacman

Open (with root) `/etc/pacman.conf` and add:

```
[hitcat-aur]
SigLevel = Optional TrustAll
Server = https://lu15ange7.gitlab.io/hitcat-aur/$arch
```
Save file and update your pacman

```
sudo pacman -Syu
```

## My packeages

### A610ProDriver [(Source Code)](https://gitlab.com/Lu15ange7/a610prodriver)
-   `a610prodriver`           : A unofficial Parblo A610 Pro driver support for GNU/Linux
-   `a610prodriver-gui`      : PanelTablet (a GUI for Parblo A610 Pro)
-   `a610prodriver-full`      : Parblo A610 Pro driver Full version (Only use for develop or testing)

### Ufurmix-pawprints (Plymouth theme) [(Source Code)](https://gitlab.com/Lu15ange7/ufurmix-pawprints)

-   `plymouth-theme-ufurmix-pawprints` : It's a theme for plymouth taked from the Ubuntu Furry Remix made by Shnatsel. [(Post Install Readme)](pkgbuilds/plymouth-ufurmix-pawprints/plymouth-ufurmix-pawprints.install)

### Copilot-Desktop [(Source Code)](https://gitlab.com/Lu15ange7/copilot-desktop)

-   `copilot-desktop` : It's a Copilot AI client desktop made with Electron

### Mini-Vantage [(Source Code)](https://gitlab.com/Lu15ange7/mini-vantage)

-   `mini-vantage` : A Lenovo Vantage for GNU/Linux.

### Many more comming soon ...

Enjoy!